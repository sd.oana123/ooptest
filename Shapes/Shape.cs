namespace OOP.Shapes
{
    interface Shape {

        double calculateArea();

        //should just print i am drawing a {0}
        double draw();
    }
}